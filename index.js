'use strict';
const drawBoard = require('./drawBoard.js');
const drawChallenge = require('./drawChallenge.js');
const drawSolution = require('./drawSolution.js');
const drawTrack = require('./drawTrack.js');

module.exports = {
  drawBoard,
  drawChallenge,
  drawSolution,
  drawTrack
};
