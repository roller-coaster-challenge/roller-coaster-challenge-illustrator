'use strict';
const fs = require('fs');

const drawChallenge = require('./src/drawChallenge.js');

if (process.argv.length !== 3) {
  console.log('One parameter expected:');
  console.log(' - Challenge file');
  process.exit(1);
}

const track = JSON.parse(fs.readFileSync(process.argv[2], 'utf8'));
fs.writeFileSync('test.svg', drawChallenge({track}), 'utf8');
