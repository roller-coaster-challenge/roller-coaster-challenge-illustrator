# Roller Coaster Challenge Illustrator

Generate printable challenge cards for Roller Coaster Challenge. It can draw tracks, challenges and solutions of the [Roller Coaster Challenge Schema](https://gitlab.com/roller-coaster-challenge/roller-coaster-challenge-schema).

![A preview of Track 06](docs/track06.png)

## Run it

To generate a SVG file, run:

```bash
node illustrator.js fixtures/track06.json
```

If you have the vector graphic editor [Inkscape](https://inkscape.org) installed, you can export the SVG to a PNG graphic:

```bash
inkscape -e tmp.png -d 1200 test.svg 
```

## Use it as a dependency

You may use the illustrator as a library in other projects. The package management is easiest with NPM.

```bash
npm install roller-coaster-challenge-illustrator
```

To generate an SVG from a track file, create a JavaScript file and paste the following into it:

```javascript
const fs = require('fs');
const illustrator = require('roller-coaster-challenge-illustrator');

// Load the track to a JavaScript object
const track = JSON.parse(fs.readFileSync('track01.json', 'utf8'));
// Generate a SVG string from the track
const svgString = illustrator.drawChallenge({track});
// Save the SVG string to a file
fs.writeFileSync('test.svg', drawChallenge({track}), 'utf8');
```

The following functions are exposed:
* `drawBoard`
* `drawChallenge`
* `drawSolution`
* `drawTrack`

The API has not been stabilized yet.

## License

[Roller Coaster Challenge](https://www.thinkfun.com/products/roller-coaster-challenge/) is a Trademark of ThinkFun and has been invented by Oli Morris. This 100% fan project is not affiliated with ThinkFun nor Oli Morris. No assets with their copyright have been used.

Copyright (C) 2019  The Roller Coaster Challenge Illustrator authors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
