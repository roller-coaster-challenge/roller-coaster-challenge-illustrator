'use strict';
function drawBoard({xLength, yLength}) {
  let res = `<rect style="fill:#bbbbbb;stroke:#333333;stroke-width:0.05"`;
  res += ` x="0" y="0" width="${xLength}" height="${yLength}" rx="0.15" ry="0.15" />`;
  res += '<g>';
  for (var y = 0; y < xLength; y++) {
    for (var x = 0; x < yLength; x++) {
      res += `<circle style="fill:none;stroke:#888888;stroke-width:0.05"`;
      res += ` cx="${x + 0.5}" cy="${y + 0.5}" r="0.35" />`;
    }
  }
  res += '</g>';
  res += '<g>';
  for (var y = 0; y < yLength; y++) {
    for (var x = 1; x < xLength; x++) {
      res += `<line style="stroke:#888888;stroke-width:0.05"`;
      res += ` x1="${x}" y1="${y + 0.2}" x2="${x}" y2="${y + 0.8}" />`;
    }
  }
  res += '</g>';
  res += '<g>';
  for (var y = 1; y < yLength; y++) {
    for (var x = 0; x < xLength; x++) {
      res += `<line style="stroke:#888888;stroke-width:0.05"`;
      res += ` x1="${x + 0.2}" y1="${y}" x2="${x + 0.8}" y2="${y}" />`;
    }
  }
  res += '</g>';
  return res;
}

module.exports = drawBoard;
