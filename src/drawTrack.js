'use strict';
const MAGENTA = '#a51497';
const GREEN = '#32b92d';
const BLUE = '#2a82d7';
const ORANGE = 'orange';
const RED = '#fc0d1b';
const DARKBLUE = '#3714a4';
const YELLOW = '#feee35';

const TRACKWIDTH = 0.334;

function directionToDegree(direction) {
  if (direction === 'N') return 0;
  if (direction === 'E') return 90;
  if (direction === 'S') return 180;
  if (direction === 'W') return 270;
}

function drawCurvedTrack({x, y, direction, turn}) {
  const rotation = directionToDegree(direction) + 180;
  let res = `<g transform="translate(${x} ${y})">`;
  if (turn === 'right') {
    res += `<line style="stroke:${RED};stroke-width:${TRACKWIDTH/2}"`;
    res += ` transform="rotate(${rotation})"`
    res += ` x1="-0.5" y1="-0.5" x2="-${TRACKWIDTH/2}" y2="-${TRACKWIDTH/2}" />`;
    res += `<path style="fill:none;stroke:${RED};stroke-width:${TRACKWIDTH}"`;
    res += ` transform="rotate(${rotation})"`
    res += ` d="m 0,-0.5 v 0.1 C 0,-0.15 -0.15,0 -0.4,0 h -0.1" />`;
    //res += ` d="M 0,-0.5 C 0,-0.2 -0.2,0 -0.5,0" />`;
  } else {
    res += `<line style="stroke:${DARKBLUE};stroke-width:${TRACKWIDTH/2}"`;
    res += ` transform="rotate(${rotation})"`
    res += ` x1="0.5" y1="-0.5" x2="${TRACKWIDTH/2}" y2="-${TRACKWIDTH/2}" />`;
    res += `<path style="fill:none;stroke:${DARKBLUE};stroke-width:${TRACKWIDTH}"`;
    res += ` transform="rotate(${rotation})"`
    res += ` d="m 0,-0.5 v 0.1 C 0,-0.15 0.15,0 0.4,0 h 0.1" />`;
    //res += ` d="M 0,-0.5 C 0,-0.2 0.2,0 0.5,0" />`;
  }
  res += '</g>';
  return res;
}

function drawEnd({x, y, direction}) {
  const rotation = directionToDegree(direction);
  let res = `<g transform="translate(${x} ${y}) rotate(${rotation})">`;
  res += `<line style="stroke:${ORANGE};stroke-width:${TRACKWIDTH}"`;
  res += ` x1="0" y1="0" x2="0" y2="0.5" />`;
  res += `<circle style="fill:white;stroke:${ORANGE};stroke-width:0.075"`;
  res += ` cx="0" cy="0" r="0.3" />`;
  res += `<circle style="fill:white;stroke:${ORANGE};stroke-width:0.075"`;
  res += ` cx="0" cy="0" r="0.15" />`;
  res += '</g>';
  return res;
}

function drawLoop({x, y, direction}) {
  const rotation = directionToDegree(direction);
  let res = `<g transform="translate(${x} ${y}) rotate(${rotation + 180})">`;
  res += `<rect style="fill:#dddd00;stroke:#bbbbbb;stroke-width:0.03"`;
  res += ` x="-1" y="3.15" width="1" height="0.7" />`;
  res += `<line style="stroke:${YELLOW};stroke-width:${TRACKWIDTH}"`;
  res += ` x1="0" y1="0.5" x2="0" y2="4" />`;
  res += `<line style="stroke:${YELLOW};stroke-width:${TRACKWIDTH};stroke-linecap:round"`;
  res += ` x1="0" y1="4" x2="-1" y2="3" />`;
  res += `<line style="stroke:${YELLOW};stroke-width:${TRACKWIDTH}"`;
  res += ` x1="-1" y1="3" x2="-1" y2="4.5" />`;
  res += '</g>';
  return res;
}

function drawPost({x, y, height}) {
  let res = `<ellipse style="fill:black"`;
  res += ` cx="${x + 0.5}" cy="${y + 0.5}" rx="0.25" ry="0.25" />`;
  res += `<text x="${x + 0.3}" y="${y + 0.65}"`
  res += ` style="font:0.4px sans-serif;fill:white">${height}</text>`;
  return res;
}

function drawStart({x, y, direction}) {
  const rotation = directionToDegree(direction);
  let res = `<g transform="translate(${x} ${y}) rotate(${rotation})">`;
  res += `<line style="stroke:${ORANGE};stroke-width:${TRACKWIDTH}"`;
  res += ` x1="0" y1="0" x2="0" y2="-0.5" />`;
  res += `<circle style="fill:${GREEN};stroke:${ORANGE};stroke-width:0.075"`;
  res += ` cx="0" cy="0" r="0.3" />`;
  res += '</g>';
  return res;
}

function drawStraightTrack({x, y, direction, length, drop}) {
  const rotation = directionToDegree(direction);
  let color = 'pink';
  if (drop === 0) color = MAGENTA;
  if (drop === 1) color = GREEN;
  if (drop === 2) color = BLUE;
  if (drop === 3) color = ORANGE;
  let res = `<g transform="translate(${x} ${y}) rotate(${rotation})">`;
  res += `<line style="stroke:${color};stroke-width:${TRACKWIDTH}"`;
  res += ` x1="0" y1="-0.5" x2="0" y2="-${length + 0.5}" />`;
  res += '</g>';
  return res;
}

function sortByHeight(a, b) {
  return a.height - b.height;
}

function drawTrack({track}) {
  // Ensure we have arrays to iterate over
  track.trackPieces = track.trackPieces || [];
  track.posts = track.posts || [];
  track.tunnels = track.tunnels || [];

  const sortedTrackPieces = track.trackPieces.sort(sortByHeight);
  let res = '';
  sortedTrackPieces.forEach((trackPiece) => {
    if (trackPiece.type === 'start') res += drawStart(trackPiece);
    if (trackPiece.type === 'end') res += drawEnd(trackPiece);
    if (trackPiece.type === 'straight') res += drawStraightTrack(trackPiece);
    if (trackPiece.type === 'curve') res += drawCurvedTrack(trackPiece);
    if (trackPiece.type === 'loop') res += drawLoop(trackPiece);
  });
  track.tunnels.forEach((tunnel) => {
    res += drawTunnel(tunnel);
  });
  track.posts.forEach((post) => {
    res += drawPost(post);
  });
  return res;
}

function drawTunnel({orientation, x, y, maxHeight}) {
  const rotation = orientation === 'vertical' ? 90 : 0;
  let color = 'pink';
  if (maxHeight === 2) color = 'purple';
  if (maxHeight === 4) color = 'red';
  let res = `<g transform="translate(${x} ${y}) rotate(${rotation})">`;
  res += `<line style="stroke:${color};stroke-width:0.1"`;
  res += ` x1="0.2" y1="0" x2="1.8" y2="0" />`;
  res += '</g>';
  return res;
}

module.exports = drawTrack;
