'use strict';
const drawBoard = require('./drawBoard.js');
const drawTrack = require('./drawTrack.js');

function drawChallenge({track}) {
  let res = '<svg viewBox="0 0 60 60" xmlns="http://www.w3.org/2000/svg">';
  res += '<defs>';
  res += '<circle id="boardPostSocket" style="fill:none;stroke:#888888;stroke-width:0.05" cx="0.5" cy="0.5" r="0.35" />';
  res += '</defs>';
  res += '<g transform="translate(5 5) scale(10 10)">'
  res += drawBoard(track);
  res += drawTrack({track});
  res += '</g>';
  res += `</svg>`;
  return res;
}

module.exports = drawChallenge;
